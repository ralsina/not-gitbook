"""Plugin that preprocesses Markdown files via pyliterate."""

# This used to just be a Makefile rule like this:
# %.run.md: %.md $(wildcard code/%.py)
#         env PYTHONPATH=. run_markdown --timeout_seconds=15 --root_dir code/ $< > $@ || (rm -f $@ && false)

# But times change.

import logging
import os
import shlex
import subprocess
from pathlib import Path


def filter(path):
    dst_path = path.with_suffix(".pyliterate")
    cwd = path.parent
    f_name = path.name
    # Some heuristics because this is just for my book, really
    root_dir = cwd / "code" / path.stem
    if not root_dir.is_dir():
        root_dir = cwd / "code"
        if not root_dir.is_dir():
            root_dir = Path(".")
    my_env = os.environ.copy()
    my_env["PYTHONPATH"] = "."
    try:
        processed = subprocess.check_output(
            shlex.split(
                f"run_markdown --timeout_seconds=15 --root_dir {root_dir.relative_to(cwd)} {f_name}"
            ),
            cwd=cwd,
            env=my_env,
        )
        dst_path.write_bytes(processed)
        return dst_path
    except Exception as e:
        logging.error(f"Error processing {path}")
        logging.exception(e)
        raise
