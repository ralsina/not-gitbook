"""Plugin to implement syntax HL based on chroma with my hackish pyliterate extensions. Probably useless for anyone else."""

import subprocess
from pathlib import Path

import mistune


def css():
    """Return CSS that needs to be linked in the page."""
    css_path = Path(__file__).parent / "chroma.css"
    return css_path.read_text()


def chain_renderer(renderer_class):
    """Takes a renderer class and creates a new one that inherits it and overrides some method."""

    class ChromaHL(renderer_class):
        def block_code(self, code, lang):
            if not lang:
                return "\n<pre><code>%s</code></pre>\n" % mistune.escape(code)

            args = [
                "chroma",
                "-l",
                lang,
                "--html-only",
                "--formatter=html",
                "--style",
                "vs",
            ]
            lines = code.split("\n")
            while lines and not lines[-1]:
                lines.pop()

            if lines and lines[0].startswith("|||"):
                # Highlighted lines
                start = lines.pop(0)
                args += ["--html-highlight=" + start[3:]]

            if lines and lines[0].startswith("&&&"):
                # Start number prefix
                start = lines.pop(0)
                args += ["--html-lines"]
                args += ["--html-base-line=" + start[3:]]
            elif len(lines) > 1:
                args += ["--html-lines"]

            code = "\n".join(lines)

            if lang == "text":
                return "\n<pre><code>%s</code></pre>\n" % mistune.escape(code)

            else:
                code = subprocess.check_output(args, input=code.encode("utf-8")).decode(
                    "utf-8"
                )

            return code

    return ChromaHL
