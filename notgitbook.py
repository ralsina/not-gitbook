#!/usr/bin/env python

"""Not-Gitbook

Usage:
  notgitbook.py build [<book>] [<output>]
  notgitbook.py pdf [<book>] [<output>]
  notgitbook.py serve <book> <output>
  notgitbook.py init <book>
  notgitbook.py mobi <book>
  notgitbook.py epub <book>
"""

import glob
import hashlib
import importlib.util
import logging
import pprint
import shlex
import shutil
import subprocess
from os import makedirs
from os.path import relpath
from pathlib import Path
from urllib.parse import urlsplit, urlunsplit

import docopt
import jinja2
import mistune
from bs4 import BeautifulSoup

logging.basicConfig(level=logging.INFO)

template_lookup = None


def resolve_url(url):
    """Like normpath for URLs, copied from
    https://stackoverflow.com/questions/2131290/how-can-i-normalize-collapse-paths-or-urls-in-python-in-os-independent-way"""

    parts = list(urlsplit(url))
    segments = parts[2].split("/")
    segments = [segment + "/" for segment in segments[:-1]] + [segments[-1]]
    resolved = []
    for segment in segments:
        if segment in ("../", ".."):
            if resolved[1:]:
                resolved.pop()
        elif segment not in ("./", "."):
            resolved.append(segment)
    parts[2] = "".join(resolved)
    return urlunsplit(parts)


def setup_jinja(book_path):
    """Configure Jinja."""
    global template_lookup
    # TODO: cache
    template_lookup = jinja2.Environment()
    template_lookup.trim_blocks = True
    template_lookup.lstrip_blocks = True
    # TODO: support theme configuration / override
    locations = [
        Path(book_path) / "templates",
        Path(__file__).parent / "themes" / "base" / "templates",
    ]
    logging.info(f"Template locations: {locations}")
    template_lookup.loader = jinja2.FileSystemLoader(locations, encoding="utf-8")


def render_template(template_name, **context):
    """Render a Jinja template from path using context."""
    template = template_lookup.get_template(template_name)
    context["plugin_css_assets"] = list(plugin_css_assets.keys())
    data = template.render(**context)
    return data


markdown = None

plugin_css_assets = {}


def setup_md_renderer_plugins():
    global markdown, plugin_css_assets
    Renderer = mistune.Renderer
    for plugin in Path(__file__).parent.glob("plugins/md_renderers/*/plugin.py"):
        p_name = f"md_renderer.{plugin.parent.name}"
        spec = importlib.util.spec_from_file_location(p_name, plugin)
        p_module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(p_module)
        Renderer = p_module.chain_renderer(Renderer)
        css = p_module.css()
        hash = hashlib.md5(css.encode("utf-8")).hexdigest()
        plugin_css_assets[hash] = css
    markdown = mistune.Markdown(renderer=Renderer(escape=False))


def compile_markdown(source):
    """Compile markdown to HTML, return a string."""
    logging.info("Compiling %s", source)
    source = preproc_md_file(source)
    try:
        html = markdown(source.read_text())
    except FileNotFoundError:
        logging.error(f"Source not found: {source}")
        html = f"MISSING FILE {source}"
    return html


preproc_filters = []


def setup_md_preproc_filters():
    """Load all the md preproc filter plugins."""
    for plugin in Path(__file__).parent.glob("plugins/md_preproc/*/plugin.py"):
        p_name = f"md_preproc.{plugin.parent.name}"
        spec = importlib.util.spec_from_file_location(p_name, plugin)
        p_module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(p_module)
        p_module.filter.name = p_name
        preproc_filters.append(p_module.filter)


def preproc_md_file(source):
    """Apply filters to the given markdown file."""
    for filter in preproc_filters:
        logging.info(f"Processing {source} with {filter.name}")
        source = filter(source)
    return source


def build_markdown(source, dest, context):
    """Compile markdown to HTML, save in dest."""
    # Make sure output folder exists
    dest_dir = dest.parent
    if not dest_dir.is_dir():
        logging.info(f"Creating {dest_dir}/")
        makedirs(dest_dir)
    content = compile_markdown(source)
    html = render_template("page.j2", content=content, **context)
    dest.write_text(html)
    logging.info("Saved to %s", dest)


def parse_summary_md(md_file):
    """Parse these lists gitbook uses in Markdown.

    Example:

    # Summary

    * [Part I](part1/README.md)
        * [Writing is nice](part1/writing.md)
        * [GitBook is nice](part1/gitbook.md)
    * [Part II](part2/README.md)
        * [We love feedback](part2/feedback_please.md)
        * [Better tools for authors](part2/better_tools.md)
    """

    # So, basically gitbook expects the following structure:
    # Parts, that start in titles
    #    Chapters, that are a list of links inside a part
    #        Subchapters, a list of links inside a chapter
    #
    # Parts are a title and a list of chapters
    # Chapters are a title, a link to a MD file, and a list of subchapters
    # Subchapters are a title and a link to a MD file

    # Idea: let's just make it HTML and parse the sucker, and not
    # bother with regexes.
    html = compile_markdown(md_file)
    doc = BeautifulSoup(html, features="html.parser")

    first_part = doc.find("ul")
    if first_part is None:
        logging.error("Summary has no content")
        return {}

    parts_list = [first_part] + first_part.find_next_siblings("ul")
    parts = []
    # The title for each part is a title before it
    for part in parts_list:
        title = part.find_previous_sibling("h1").text
        first_chapter = part.find("li")
        if first_chapter is None:
            continue

        chapters_list = [first_chapter] + first_chapter.find_next_siblings("li")
        chapters = []
        for chapter in chapters_list:
            item = chapter.find("a")
            chapter_title = item.text
            chapter_link = resolve_url(item.attrs["href"])
            # Yes, this will "support" deeper nesting by breaking it
            # I do not care at all.
            subchapters = chapter.find("ul")
            if subchapters:
                subchapters = [
                    [x.text, resolve_url(x.attrs["href"])]
                    for x in subchapters.find_all("a")
                ]
            else:
                subchapters = []
            chapters.append([chapter_title, chapter_link, subchapters])
        parts.append([title, chapters])

    # pretty-log it
    for part, chapters in parts:
        logging.info(f"part {part}")
        for title, md_file, subchapters in chapters:
            logging.info(f"  chapter: {title} - {md_file}")
            for title, md_file in subchapters:
                logging.info(f"    subchapter: {title} - {md_file}")
    return parts


def build(book_path, dest_path, pdf=False):
    """Build a book."""
    logging.info("Building book in %s", book_path)

    # Make sure input exists
    summary = book_path / "SUMMARY.md"
    if not summary.is_file:
        logging.error(f"Missing summary: {summary}")
        raise FileNotFoundError(summary)

    # Make sure output folder exists
    if not dest_path.is_dir():
        logging.info(f"Creating {dest_path}")
        makedirs(dest_path)

    # Setup internal components
    setup_jinja(book_path)
    setup_md_preproc_filters()
    setup_md_renderer_plugins()

    # Parse summary document
    parts = parse_summary_md(summary)

    # Create linear list of chapters/subchapters
    flat_list = []
    for _, chapters in parts:
        # Part itself doesn't have content
        for title, md_file, subchapters in chapters:
            # FIXME: do proper extension replacement
            md_dest_path = dest_path / md_file.replace(".md", ".html")
            offset = md_file.count("/")
            flat_list.append(
                [
                    book_path / md_file,
                    md_dest_path,
                    {"summary": parts, "offset": offset, "title": title},
                ]
            )
            for title, md_file in subchapters:
                offset = md_file.count("/")
                # FIXME: do proper extension replacement
                md_dest_path = dest_path / md_file.replace(".md", ".html")
                flat_list.append(
                    [
                        book_path / md_file,
                        md_dest_path,
                        {"summary": parts, "offset": offset, "title": title},
                    ]
                )

    # Add previous chapter data to context
    for i, chapter in enumerate(flat_list[1:], 1):
        cur_context = chapter[2]
        prev_chapter = flat_list[i - 1]
        cur_context["prev_title"] = prev_chapter[2]["title"]
        cur_context["prev"] = relpath(prev_chapter[1], chapter[1].parent)

    # Add next chapter data to context
    for i, chapter in enumerate(flat_list[:-1]):
        cur_context = chapter[2]
        next_chapter = flat_list[i + 1]
        cur_context["next_title"] = next_chapter[2]["title"]
        cur_context["next"] = relpath(next_chapter[1], chapter[1].parent)

    # Render all chapters and subchapters
    for src, dest, context in flat_list:
        build_markdown(src, dest, context=context)

    if pdf:
        single_html = dest_path / "full_book.html"
        # Render all chapters / subchapters as a single file (experimental)
        full_book = []
        for src, dest, context in flat_list:
            with open(dest) as inf:
                doc = BeautifulSoup(inf.read(), features="html.parser")
                content = doc.find("section")
                # TODO Patch links
                # * Need to fix depth of links because it's now flat
                # * Need to resolve links to other output files so they point to
                #   the same file now (ebook-convert drags all those links otherwise)
                # Prefix that needs to be added to all references:
                orig_dest_dir = dest.parent
                prefix = orig_dest_dir.relative_to(dest_path)
                # Fix images.
                for img in content.find_all("img"):
                    # FIXME: assumes dir separator is / ?
                    if not img["src"].startswith("http"):
                        img["src"] = str(prefix / img["src"])

                full_book.append(str(content))

        content = "\n\n\n".join(full_book)
        pdf_path = dest_path / "full_book.pdf"
        open(single_html, "w").write(render_template("singlepage.j2", content=content))
        subprocess.check_call(
            shlex.split(
                f"ebook-convert {single_html} {pdf_path} --max-levels=0 --pdf-default-font-size=12 --embed-all-fonts"
            )
        )

    # Copy all relevant assets
    # First, the theme
    # TODO support themes properly
    copy_tree(
        Path(__file__).parent / "themes" / "base" / "assets", dest_path / "assets"
    )
    # Now CSS from plugins
    dest_dir = dest_path / "styles"
    if not dest_dir.is_dir():
        logging.info(f"Creating {dest_dir}/")
        makedirs(dest_dir)
    for k, v in plugin_css_assets.items():
        dst = dest_path / "styles" / (k + ".css")
        dst.write_text(v)

    # Now copy the source tree (includes images and so on)
    copy_tree(book_path, dest_path)


def copy_tree(src, dst):
    """Copy directory tree src to dst, overwrite anything."""
    l = len(str(src)) + 1
    d_list = [str(d)[l:] for d in src.glob("**/")]
    for d in d_list:
        if d:
            makedirs(dst / d, exist_ok=True)
    f_list = [f for f in src.glob("**") if not f.is_dir()]
    for f in f_list:
        f_dst = dst / str(f)[l:]
        logging.info(f"Copying {f} -> {f_dst}")
        shutil.copy2(f, f_dst)


if __name__ == "__main__":
    arguments = docopt.docopt(__doc__, version="Not-Gitbook v0.0.1")
    if arguments["build"] or arguments["pdf"]:
        book_path = arguments.get("<book>") or "."
        dest_path = arguments.get("<output>") or "_book"
        build(Path(book_path), Path(dest_path), pdf=arguments["pdf"])
