# not-gitbook

Something that works like gitbook but is not gitbook because gitbook is an endless source of frustration and every time I sit down to write [my book](https://ralsina.gitlab.io/boxes-book/) I end up fighting it.

Really, this is probably the worst code I wrote in the past couple of years. So, hey, if you want to know how "well" I code? Better than this :-)

OTOH, it does *more or less* the same as all [this code](https://github.com/GitbookIO/gitbook) ...